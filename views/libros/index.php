<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1> <span class="glyphicon glyphicon-book" aria-hidden="true"></span> <?= Html::encode($this->title) ?></h1>
     

    <p>
        <?php 
         //echo Html::a('Create Libros', ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [ 'class'  =>  'table tabla1 table-striped table-bordered' ],
        'summary'=>'pág. {page} de {pageCount} <br> Total de autores: {totalCount} <br>',
        
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'titulo',
            //'foto',
            [
        'attribute' => 'portada',
        'format' => 'html',    
        'value' => function ($data) {
            return Html::img(Yii::getAlias('@web').'/imags/'. $data['foto'],
                ['width' => '150px']);
        },
    ],

            //['class' => 'yii\grid\ActionColumn'], para quitar los botones que editan
        ],
    ]); ?>
    <?= Html::img('@web/imags/1.jpg', ['alt' => 'George Orwell']) ?>
</div>
